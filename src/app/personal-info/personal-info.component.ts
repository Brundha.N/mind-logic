import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {

loginForm = new FormGroup({
		title: new FormControl({value:'',disabled:true}),
		mail: new FormControl({value:'sjayakumar@mindlogic.com',disabled:true}),
		phone: new FormControl({value:'',disabled:true}),
		location: new FormControl({value:'',disabled:true}),
		timeZone: new FormControl({value:'(GMT+05.00) Ekaterinburg',disabled:true}),
		secure:new FormGroup({
			currentPassword: new FormControl('',Validators.required),
			newPassword: new FormControl('',Validators.required),
			confimPassword: new FormControl('',Validators.required)
		})
	});
stateBeforeEdit={title:'',phone:'',location:''};
saved=false;
timeZoneCopy=this.loginForm.controls['timeZone'].value;
mailCopy=this.loginForm.controls['mail'].value;
percentage=0;
checkStrong=0;
details='';
correct=true;
inCorrect=true;
show={title:true,phone:true,location:true};

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * checkDetails used to print form values
   */
  checkDetails(){
    this.details=JSON.stringify(this.loginForm.getRawValue());
  }

 /**
 * editInput used to enable editing
 * @param {[type]} data [description]
 */
  editInput(data){
    let id=data.currentTarget.parentElement.id;
  	this.loginForm.controls[id].enable();
    this.show[id]=false;
    this.stateBeforeEdit[id]=this.loginForm.controls[id].value;
  }

  /**
   * saveInput used to save content
   * @param {[type]} data [description]
   */
  saveInput(data){
    this.saved=true;
    let id=data.currentTarget.parentElement.id;
    this.show[id]=true;
    this.loginForm.controls[id].disable();
    this.stateBeforeEdit[id]=this.loginForm.controls[id].value;
  }

  /**
   * discardInput used to discard Changes
   * @param {[type]} data [description]
   */
  discardInput(data){
    let id=data.currentTarget.parentElement.id;
    this.show[id]=true;
    this.loginForm.controls[id].disable();
    this.loginForm.controls[id].setValue(this.stateBeforeEdit[id]);
  }

/**
 * checkPassword used to check password stength capability
 */
  checkPassword(){
    let value=this.loginForm.controls.secure.value.newPassword;
    this.percentage=0;
    this.percentage=/\d/.test(value)?this.percentage+20:this.percentage;
    this.percentage=/[A-Z]/.test(value)?this.percentage+20:this.percentage;
    this.percentage=/[a-z]/.test(value)?this.percentage+20:this.percentage;
    this.percentage=/[_\-\$&@]/.test(value)?this.percentage+20:this.percentage;
    this.percentage=/.{8,20}/.test(value)?this.percentage+20:this.percentage;
  }

/**
 * savePassword used to compare the password
 */
  savePassword(){
    if(this.loginForm.controls.secure.value.newPassword==this.loginForm.controls.secure.value.confimPassword){
      this.inCorrect=false;
    }
  }

/**
 * discardPassword used to discard password entered
 */
  discardPassword(){
    this.loginForm['controls'].secure['controls'].confimPassword.setValue('');
  }
}
